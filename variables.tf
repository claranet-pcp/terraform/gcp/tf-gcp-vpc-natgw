#
# Project
#
variable "project_id" {
  description = "GCP Project ID"
  type        = "string"
}

#
# Network
#
variable "name" {
  description = "The first portion of the interpolated name for resources created by this module"
  type        = "string"
}

variable "routing_mode" {
  description = "Network routing mode. Default (GLOBAL)"
  type        = "string"
  default     = "GLOBAL"
}

variable "nat_mode" {
  description = "Associate NAT Gateway with VPC. Default (enabled)"
  type        = "string"
  default     = "enabled"
}

variable "subnets" {
  description = "The CIDR block for the main subnetwork"
  type        = "list"

  default = [
    {
      subnet_region     = "europe-west1"   # Region
      subnet_main       = "192.168.0.0/20" # CIDR - Main subnet
      subnet_pods       = "10.0.0.0/14"    # CIDR - Pods subnet
      subnet_services   = "10.4.0.0/20"    # CIDR - Services subnet
      sunnet_router_asn = "64512"          # AS Number of for Cloud Router within the region
    },
  ]
}
