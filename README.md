# tf-gcp-vpc-natgw
Terraform Module for GCP - VPC with NAT Gateways

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | The first portion of the interpolated name for resources created by this module | string | n/a | yes |
| nat\_mode | Associate NAT Gateway with VPC. Default (enabled) | string | `"enabled"` | no |
| project\_id | GCP Project ID | string | n/a | yes |
| routing\_mode | Network routing mode. Default (GLOBAL) | string | `"GLOBAL"` | no |
| subnets | The CIDR block for the main subnetwork | list | `<list>` | no |

## Outputs

| Name | Description |
|------|-------------|
| network | Network or VPC name |
| subnets | List of created subnets within the VPC |

