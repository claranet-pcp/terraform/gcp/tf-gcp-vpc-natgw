#
# Networking - VPC
#
resource "google_compute_network" "network" {
  name                    = var.name
  auto_create_subnetworks = false
  routing_mode            = var.routing_mode
  project                 = var.project_id
}

#
# Networking - Sub-Network with NAT Gateways
#
resource "google_compute_address" "natgw" {
  count  = var.nat_mode == "enabled" ? length(var.subnets) * 2 : 0
  name   = "${var.name}-${lookup(var.subnets[floor(count.index / 2)], "subnet_region")}-natgw-${count.index}"
  region = lookup(var.subnets[floor(count.index / 2)], "subnet_region")
}

resource "google_compute_router_nat" "natgw" {
  count                              = var.nat_mode == "enabled" ? length(var.subnets) : 0
  name                               = "${var.name}-${lookup(var.subnets[count.index], "subnet_region")}-natgw-${count.index}"
  router                             = element(google_compute_router.natgw.*.name, count.index)
  region                             = lookup(var.subnets[count.index], "subnet_region")
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [element(google_compute_address.natgw.*.self_link, count.index * 2), element(google_compute_address.natgw.*.self_link, count.index * 2 + 1)]
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

resource "google_compute_router" "natgw" {
  count   = var.nat_mode == "enabled" ? length(var.subnets) : 0
  name    = "${var.name}-${lookup(var.subnets[count.index], "subnet_region")}-router"
  network = google_compute_network.network.self_link
  region  = element(google_compute_subnetwork.default.*.region, count.index)

  bgp {
    asn = lookup(var.subnets[count.index], "subnet_router_asn", "64512")
  }
}

resource "google_compute_subnetwork" "default" {
  count                    = length(var.subnets)
  name                     = "${var.name}-${lookup(var.subnets[count.index], "subnet_region")}"
  network                  = google_compute_network.network.self_link
  region                   = lookup(var.subnets[count.index], "subnet_region")
  private_ip_google_access = lookup(var.subnets[count.index], "subnet_private_access", "false")
  enable_flow_logs         = lookup(var.subnets[count.index], "subnet_flow_logs", "false")
  ip_cidr_range            = lookup(var.subnets[count.index], "subnet_main")

  secondary_ip_range {
    range_name    = "${var.name}-${lookup(var.subnets[count.index], "subnet_region")}-pods"
    ip_cidr_range = lookup(var.subnets[count.index], "subnet_pods")
  }

  secondary_ip_range {
    range_name    = "${var.name}-${lookup(var.subnets[count.index], "subnet_region")}-services"
    ip_cidr_range = lookup(var.subnets[count.index], "subnet_services")
  }
}
