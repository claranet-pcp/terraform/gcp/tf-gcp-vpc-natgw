package test

import (
	"github.com/gruntwork-io/terratest/modules/gcp"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"testing"
)

func TestTerraformGcpVpcWithNatgw(t *testing.T) {
	t.Parallel()

	// Setup values for our Terraform apply
	projectId := gcp.GetGoogleProjectIDFromEnvVar(t)
	randomValidGcpName := gcp.RandomValidGcpName()

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code instances located
		TerraformDir: "../",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"name":       randomValidGcpName,
			"project_id": projectId,
		},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

}

func TestTerraformGcpVpcWithoutNatgw(t *testing.T) {
	t.Parallel()

	// Setup values for our Terraform apply
	projectId := gcp.GetGoogleProjectIDFromEnvVar(t)
	randomValidGcpName := gcp.RandomValidGcpName()

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code instances located
		TerraformDir: "../",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"name":       randomValidGcpName,
			"nat_mode":   "disabled",
			"project_id": projectId,
		},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

}
