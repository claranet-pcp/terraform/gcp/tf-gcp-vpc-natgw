output "network" {
  description = "Network or VPC name"
  value       = google_compute_network.network.name
}

output "subnets" {
  description = "List of created subnets within the VPC"
  value       = google_compute_subnetwork.default.*.self_link
}
